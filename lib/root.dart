import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:meteo_app/presentation/common/app_bloc_provider.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';
import 'package:meteo_app/presentation/pages/login/login.bloc.dart';
import 'package:meteo_app/presentation/pages/login/login.page.dart';
import 'package:meteo_app/presentation/pages/login/login.states.dart';
import 'package:meteo_app/presentation/pages/weather/weather.page.dart';
import 'package:meteo_app/presentation/widgets/languages/language_listener.dart';

class Root extends StatelessWidget {
  final _loginBloc = LoginBloc();

  @override
  Widget build(BuildContext context) {
    return LanguageListener(
      child: MaterialApp(
        title: 'MeteoApp',
        theme: ThemeData(
          pageTransitionsTheme: PageTransitionsTheme(builders: {
            TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
            TargetPlatform.iOS: FadeUpwardsPageTransitionsBuilder()
          }),
          primarySwatch: Colors.blue,
        ),
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        debugShowCheckedModeBanner: false,
        home: AppBlocProvider<LoginBloc, BlocState>(
          bloc: _loginBloc,
          builder: (context, state) {
            if (state is LoggedIn) {
              return WeatherPage();
            }

            return LoginPage();
          },
        ),
      ),
    );
  }
}
