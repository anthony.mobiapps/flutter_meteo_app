import 'package:geolocator/geolocator.dart';
import 'package:meteo_app/config/dependencies.dart';
import 'package:meteo_app/domain/entities/weather.entity.dart';
import 'package:meteo_app/domain/repositories/iweather.repository.dart';

class WeatherUseCases {
  IWeatherRepository _repository = serviceLocator<IWeatherRepository>();

  Future<WeatherModel?> getWeatherByCity(String cityName) => _repository.getWeatherByCity(cityName);

  Future<WeatherModel?> getWeatherByPosition(Position position) => _repository.getWeatherByPosition(position);
}
