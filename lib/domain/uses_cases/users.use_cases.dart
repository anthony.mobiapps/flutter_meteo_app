import 'package:meteo_app/config/dependencies.dart';
import 'package:meteo_app/domain/entities/user.entity.dart';
import 'package:meteo_app/domain/repositories/iuser.repository.dart';

class UsersUseCases {
  IUserRepository _repository = serviceLocator<IUserRepository>();

  Future<void> login(String email, String password) => _repository.login(email, password);

  Future<void> logout() => _repository.logout();

  User getLoggedUser() =>_repository.getLoggedUser();
}
