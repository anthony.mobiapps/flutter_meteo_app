class User {
  int? id;
  String email;
  String password;
  String? firstName;
  String? lastName;
  String? sessionToken;

  User({this.id, required this.email, required this.password, this.firstName, this.lastName, this.sessionToken});
}
