import 'package:meteo_app/data/dtos/weather.dto.dart';

class WeatherModel {
  final City city;
  final Map<DateTime, List<Prevision>> previsions;

  WeatherModel({required this.city, required this.previsions});
}