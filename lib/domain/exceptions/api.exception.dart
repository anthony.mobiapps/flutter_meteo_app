import 'package:easy_localization/easy_localization.dart';
import 'package:meteo_app/domain/exceptions/basic.exception.dart';

class ApiException extends BasicException {
  ApiException(String message) : super(message);

  ApiException.fromStatusCode(int code) : super(messageFromCode(code));

  static String messageFromCode(int code) {
    switch (code) {
      case 404:
        return tr('errors.cityNotFound');
    }

    return tr('errors.unknownError');
  }
}
