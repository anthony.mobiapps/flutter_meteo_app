import 'package:meteo_app/domain/exceptions/basic.exception.dart';

class RepositoryException extends BasicException {
  RepositoryException(String message) : super(message);
}
