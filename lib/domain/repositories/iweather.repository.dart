import 'package:geolocator/geolocator.dart';
import 'package:meteo_app/domain/entities/weather.entity.dart';

abstract class IWeatherRepository {
  Future<WeatherModel?> getWeatherByCity(String cityName);

  Future<WeatherModel?> getWeatherByPosition(Position position);
}
