import 'package:meteo_app/domain/entities/user.entity.dart';

abstract class IUserRepository {
  Future<void> login(String email, String password);
  Future<void> logout();
  User getLoggedUser();
}
