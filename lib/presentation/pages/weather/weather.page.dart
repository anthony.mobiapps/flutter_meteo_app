import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import "package:collection/collection.dart";
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meteo_app/config/parameters.dart';
import 'package:meteo_app/domain/entities/weather.entity.dart';
import 'package:meteo_app/presentation/common/app_bloc_provider.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';
import 'package:meteo_app/presentation/pages/login/login.bloc.dart';
import 'package:meteo_app/presentation/pages/weather/weather.bloc.dart';
import 'package:meteo_app/presentation/pages/weather/weather.states.dart';
import 'package:meteo_app/presentation/widgets/languages/language.bloc.dart';
import 'package:meteo_app/presentation/widgets/weather_display.widget.dart';

class WeatherPage extends StatefulWidget {
  @override
  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  LanguageBloc? _languageBloc;
  final _bloc = WeatherBloc();
  WeatherModel? _localWeather;
  WeatherModel? _searchedWeather;
  Position? _position;
  Future<Position>? _location;
  int _currentSlideIdx = 0;

  @override
  void initState() {
    _languageBloc = BlocProvider.of<LanguageBloc>(context);
    _getPosition();
    _fetchSearchedWeather();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LanguageBloc, BlocState>(
      bloc: _languageBloc,
      listener: (context, state) {
        _fetchLocalWeather();
        _fetchSearchedWeather();
      },
      child: AppBlocProvider<WeatherBloc, BlocState>(
        bloc: _bloc,
        listener: (context, state) {
          if (state is LocalWeatherLoaded) {
            _localWeather = state.weather;
          }

          if (state is SearchedWeatherLoaded) {
            _searchedWeather = state.weather;
          }
        },
        builder: (context, state) => Scaffold(
            appBar: AppBar(
              title: Text(tr('weatherPage.title',
                  namedArgs: {'firstName': BlocProvider.of<LoginBloc>(context).getLoggedUser().firstName!})),
              actions: [_buildLanguageMenu(context), _buildLogoutIcon(context)],
            ),
            body: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(AppParameters.instance.weatherBackground),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop))),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.05,
                    vertical: MediaQuery.of(context).size.height * 0.03),
                child: Column(
                  children: [
                    Expanded(
                      child: CarouselSlider(
                        items: [_buildLocalWeather(), _buildSearchWeather()],
                        options: CarouselOptions(
                            onPageChanged: (index, reason) => setState(() => _currentSlideIdx = index),
                            viewportFraction: 1,
                            height: double.infinity,
                            enableInfiniteScroll: false,
                            autoPlay: false),
                      ),
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      _buildCircleButton(_currentSlideIdx == 0),
                      _buildCircleButton(_currentSlideIdx == 1)
                    ]),
                  ],
                ),
              ),
            )),
      ),
    );
  }

  Widget _buildCircleButton(bool active) => Container(
        width: 8,
        height: 8,
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle, color: active ? Colors.black.withOpacity(0.9) : Colors.black.withOpacity(0.4)),
      );

  Widget _buildLocalWeather() {
    return FutureBuilder<Position>(
        future: _location,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return _loadingWidget(true);
          if (_position == null ||
              (_position != null && snapshot.data!.longitude != _position!.longitude ||
                  snapshot.data!.latitude != _position!.latitude)) {
            _position = snapshot.data;
            _fetchLocalWeather();
          }
          if (_localWeather == null) return _loadingWidget(true);
          return WeatherDisplay(
            local: true,
            weather: _localWeather!,
            refreshCallback: _fetchLocalWeather,
          );
        });
  }

  Future<Position> _getPosition() => _location = Geolocator.getCurrentPosition();

  Widget _buildSearchWeather() {
    return _searchedWeather == null
        ? _loadingWidget()
        : WeatherDisplay(weather: _searchedWeather!, refreshCallback: _fetchSearchedWeather);
  }

  Widget _loadingWidget([bool geolocation = false]) => Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          geolocation
              ? Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text(tr('weatherPage.geolocating')),
                )
              : Container(),
          CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.black), strokeWidth: 2),
        ],
      ));

  Widget _buildLanguageMenu(BuildContext context) {
    final current = context.locale;
    final others = context.supportedLocales.where((locale) => locale != current);
    final List<Locale> locales = [current, ...others];
    return PopupMenuButton<Locale>(
      itemBuilder: (context) => locales
          .mapIndexed((index, locale) =>
              PopupMenuItem(child: Text(tr('languages.${locale.languageCode}')), enabled: index != 0, value: locale))
          .toList(),
      icon: Icon(Icons.language),
      onSelected: (selectedLocale) => _languageBloc!.setLocale(selectedLocale),
    );
  }

  Widget _buildLogoutIcon(BuildContext context) =>
      IconButton(icon: Icon(Icons.logout), onPressed: () => BlocProvider.of<LoginBloc>(context).logout());

  Future<void> _fetchSearchedWeather() => _bloc.getWeather(cityName: _bloc.searchedCity);

  Future<void> _fetchLocalWeather() async {
    _getPosition();
    _bloc.getWeather(position: _position);
  }
}
