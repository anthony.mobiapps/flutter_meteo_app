import 'package:meteo_app/domain/entities/weather.entity.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';

abstract class WeatherLoaded extends BlocState {
  final int _privateTimeStamp = DateTime.now().millisecondsSinceEpoch;
  final WeatherModel weather;

  WeatherLoaded(this.weather);

  @override
  List<Object> get props => [weather, _privateTimeStamp];
}

class LocalWeatherLoading extends Loading {}

class SearchedWeatherLoading extends Loading {}

class LocalWeatherLoaded extends WeatherLoaded {
  LocalWeatherLoaded(WeatherModel weather) : super(weather);
}

class SearchedWeatherLoaded extends WeatherLoaded {
  SearchedWeatherLoaded(WeatherModel weather) : super(weather);
}
