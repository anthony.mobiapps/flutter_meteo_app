import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meteo_app/domain/uses_cases/weather.use_cases.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';
import 'package:meteo_app/presentation/pages/weather/weather.states.dart';

class WeatherBloc extends Cubit<BlocState> {
  String? _searchedCity = 'Paris';
  String? _previousSearchedCity;

  final WeatherUseCases _weatherUseCases = WeatherUseCases();

  WeatherBloc() : super(Loading());

  set searchedCity(String? city) {
    _previousSearchedCity = _searchedCity;
    _searchedCity = city;
  }

  String? get searchedCity => _searchedCity;

  void resetSearchedCity() => _searchedCity = _previousSearchedCity;

  Future<void> getWeather({String? cityName, Position? position}) async {
    assert((cityName != null) ^ (position != null));

    if (cityName != null) {
      emit(SearchedWeatherLoading());
      final previsions = await _weatherUseCases.getWeatherByCity(cityName).catchError((error) {
        resetSearchedCity();
        emit(ErrorState.from(error));
      });
      if (previsions != null) emit(SearchedWeatherLoaded(previsions));
    }

    if (position != null) {
      emit(LocalWeatherLoading());
      final previsions = await _weatherUseCases.getWeatherByPosition(position).catchError((error) {
        emit(ErrorState.from(error));
      });
      if (previsions != null) emit(LocalWeatherLoaded(previsions));
    }
  }
}
