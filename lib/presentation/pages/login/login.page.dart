import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo_app/config/parameters.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';
import 'package:meteo_app/presentation/common/validators.dart';
import 'package:meteo_app/presentation/pages/login/login.bloc.dart';
import 'package:meteo_app/presentation/widgets/app_button.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  String? email;
  String? password;
  bool _isLogging = false;

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, BlocState>(
      listener: (context, state) => _isLogging = state is CommittingState,
      child: Scaffold(
        body: SafeArea(
            child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height * 0.1, horizontal: MediaQuery.of(context).size.width * 0.1),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Spacer(flex: 3),
                Image.asset(AppParameters.instance.weatherIcon, width: MediaQuery.of(context).size.width * 0.4),
                Spacer(flex: 1),
                TextFormField(
                  decoration: InputDecoration(hintText: tr('loginPage.email')),
                  textInputAction: TextInputAction.next,
                  validator: Validators.emailRequired,
                  onSaved: (value) => email = value,
                  keyboardType: TextInputType.emailAddress,
                ),
                Spacer(flex: 1),
                TextFormField(
                    decoration: InputDecoration(hintText: tr('loginPage.password')),
                    textInputAction: TextInputAction.done,
                    validator: Validators.stringRequired,
                    onSaved: (value) => password = value,
                    keyboardType: TextInputType.text,
                    obscureText: true),
                Spacer(flex: 1),
                AppButton(text: tr('loginPage.action'), onPressed: () => _validate(context), committing: _isLogging),
                Spacer(flex: 3)
              ],
            ),
          ),
        )),
      ),
    );
  }

  void _validate(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      BlocProvider.of<LoginBloc>(context).login(email!, password!);
    }
  }
}
