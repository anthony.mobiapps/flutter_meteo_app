import 'package:meteo_app/presentation/common/bloc_state.dart';

class LoggedOut extends BlocState {
  @override
  List<Object> get props => [];
}

class LoggedIn extends BlocState {
  @override
  List<Object> get props => [];
}
