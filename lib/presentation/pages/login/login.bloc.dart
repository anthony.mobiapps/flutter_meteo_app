import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo_app/domain/entities/user.entity.dart';
import 'package:meteo_app/domain/uses_cases/users.use_cases.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';
import 'package:meteo_app/presentation/pages/login/login.states.dart';

class LoginBloc extends Cubit<BlocState> {
  final UsersUseCases _usersUseCases = UsersUseCases();

  LoginBloc() : super(LoggedOut());

  void login(String email, String password) async {
    emit(CommittingState());

    try {
      await _usersUseCases.login(email, password);
    } catch (error) {
      emit(ErrorState.from(error));
      return;
    }
    emit(LoggedIn());
  }

  void logout() async {
    await _usersUseCases.logout();
    emit(LoggedOut());
  }

  User getLoggedUser() => _usersUseCases.getLoggedUser();
}
