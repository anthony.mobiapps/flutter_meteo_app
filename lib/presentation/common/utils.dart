class Utils {
  static DateTime toDateTime(int? datetime) => DateTime.fromMillisecondsSinceEpoch(datetime! * 1000);

  static DateTime onlyDate(DateTime datetime) => DateTime(datetime.year, datetime.month, datetime.day);
}
