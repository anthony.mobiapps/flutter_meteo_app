import 'package:easy_localization/easy_localization.dart';
import 'package:email_validator/email_validator.dart';

class Validators {
  static String? stringRequired(String? value) => (value?.isNotEmpty ?? false) ? null : tr('errors.requiredField');

  static String? emailRequired(String? value) {
    final empty = value?.isEmpty ?? true;
    if (empty) {
      return tr('errors.requiredField');
    }
    if (!EmailValidator.validate(value!)) {
      return tr('errors.invalidEmail');
    }
    return null;
  }
}
