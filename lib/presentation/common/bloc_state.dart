import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

abstract class BlocState extends Equatable {}

class ErrorState extends BlocState {
  final String error;
  final int _privateTimeStamp = DateTime.now().millisecondsSinceEpoch;

  static ErrorState from(dynamic error) {
    if (error is DioError) {
      return ErrorState._(error.error.toString());
    }
    return ErrorState._(error.toString());
  }

  ErrorState._(this.error);

  @override
  List<Object> get props => [error, _privateTimeStamp];
}

class Loading extends BlocState {
  final int _privateTimeStamp = DateTime.now().millisecondsSinceEpoch;

  @override
  List<Object> get props => [_privateTimeStamp];
}

class SuccessState extends BlocState {
  final String message;
  final int _privateTimeStamp = DateTime.now().millisecondsSinceEpoch;

  SuccessState(this.message);

  @override
  List<Object> get props => [message, _privateTimeStamp];
}

class CommittingState extends BlocState {
  final int _privateTimeStamp = DateTime.now().millisecondsSinceEpoch;

  @override
  List<Object> get props => [_privateTimeStamp];
}