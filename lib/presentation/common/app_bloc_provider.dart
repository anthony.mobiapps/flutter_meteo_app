import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';

typedef Builder<STATE extends BlocState> = Widget Function(BuildContext context, STATE state);

typedef Listener<STATE extends BlocState> = void Function(BuildContext context, STATE state);

class AppBlocProvider<BLOC extends Cubit<STATE>, STATE extends BlocState> extends StatelessWidget {
  final BLOC bloc;
  final Listener<STATE>? listener;
  final Builder<STATE> builder;

  AppBlocProvider({required this.builder, this.listener, required this.bloc});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (BuildContext context) => this.bloc,
        child: BlocConsumer<BLOC, STATE>(
            listener: (BuildContext context, STATE state) {
              if (state is ErrorState) {
                _showSnackBar(context, state.error, Colors.red);
              } else if (state is SuccessState) {
                _showSnackBar(context, state.message, Colors.green);
              }
              if (this.listener != null) {
                this.listener!(context, state);
              }
            },
            builder: (BuildContext context, STATE state) => this.builder(context, state)));
  }

  void _showSnackBar(BuildContext context, String text, Color color) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(text),
      backgroundColor: color,
    ));
  }
}
