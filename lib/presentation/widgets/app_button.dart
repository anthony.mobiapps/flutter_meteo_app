import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final VoidCallback onPressed;
  final bool? committing;
  final String text;

  AppButton({required this.onPressed, this.committing = false, required this.text});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: committing! ? null : onPressed,
        child: committing!
            ? SizedBox(
                width: 20,
                height: 20,
                child:
                    CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.black), strokeWidth: 2),
              )
            : Text(text));
  }
}
