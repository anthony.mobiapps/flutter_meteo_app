import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:meteo_app/config/parameters.dart';
import 'package:meteo_app/data/dtos/weather.dto.dart';
import "package:meteo_app/presentation/common/string.extension.dart";
import 'package:meteo_app/presentation/common/utils.dart';

class PrevisionTime extends StatelessWidget {
  final Prevision prevision;
  final DateTime _dateTime;

  PrevisionTime({required this.prevision}) : _dateTime = Utils.toDateTime(prevision.dt);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(children: [
        Image.network('${AppParameters.instance.apiIconsUrl}${prevision.weather!.first.icon}.png'),
        SizedBox(width: 10),
        Text(tr('weatherPage.temperature', namedArgs: {'value' : prevision.main!.temp!.toStringAsFixed(0)})),
        Text(DateFormat(tr('format.time')).format(_dateTime)),
        Text(prevision.weather!.first.description!.capitalize(), textAlign: TextAlign.center)
      ]),
    );
  }
}
