import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo_app/domain/entities/weather.entity.dart';
import 'package:meteo_app/presentation/pages/weather/weather.bloc.dart';
import 'package:meteo_app/presentation/widgets/prevision_day.widget.dart';

class WeatherDisplay extends StatefulWidget {
  final WeatherModel weather;
  final bool local;
  final Future<void> Function() refreshCallback;

  WeatherDisplay({required this.weather, this.local = false, required this.refreshCallback});

  @override
  _WeatherDisplayState createState() => _WeatherDisplayState();
}

class _WeatherDisplayState extends State<WeatherDisplay> {
  WeatherBloc? _weatherBloc;

  @override
  void initState() {
    _weatherBloc = BlocProvider.of<WeatherBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) => RefreshIndicator(
        onRefresh: widget.refreshCallback,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            GestureDetector(
                child: _buildCityName(context), onTap: widget.local ? null : () => _cityNameDialog(context)),
            _buildWeatherDays(),
          ],
        ),
      );

  Widget _buildCityName(BuildContext context) => Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.03),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.local
                ? Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Icon(Icons.near_me, color: Colors.black.withOpacity(0.6)),
                  )
                : Container(),
            Flexible(
              child: Text(widget.weather.city.name!,
                  style: Theme.of(context).textTheme.headline4,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center),
            ),
          ],
        ),
      );

  void _cityNameDialog(BuildContext context) {
    final ctrl = TextEditingController(text: _weatherBloc!.searchedCity ?? '');
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: Text(tr('weatherPage.searchCity')),
              content: TextField(controller: ctrl, keyboardType: TextInputType.text),
              actions: [
                ValueListenableBuilder<TextEditingValue>(
                  valueListenable: ctrl,
                  builder: (context, value, child) => ElevatedButton(
                    child: Text(tr('weatherPage.validate')),
                    onPressed: value.text.isEmpty
                        ? null
                        : () {
                            _weatherBloc!.searchedCity = value.text;
                            Navigator.of(context).pop();
                            widget.refreshCallback();
                          },
                  ),
                )
              ],
            ));
  }

  Widget _buildWeatherDays() {
    return Expanded(
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 300),
        child: ListView.separated(
            key: UniqueKey(),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              final day = widget.weather.previsions.keys.elementAt(index);
              return PrevisionDay(day: day, previsions: widget.weather.previsions[day]!);
            },
            separatorBuilder: (context, index) => Divider(color: Colors.grey),
            itemCount: widget.weather.previsions.length),
      ),
    );
  }
}
