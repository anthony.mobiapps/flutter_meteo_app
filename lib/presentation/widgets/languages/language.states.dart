import 'package:flutter/material.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';

class LocaleChanged extends BlocState {
  final Locale locale;

  LocaleChanged(this.locale);

  @override
  List<Object> get props => [locale];
}
