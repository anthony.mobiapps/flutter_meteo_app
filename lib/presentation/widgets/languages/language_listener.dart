import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo_app/config/dependencies.dart';
import 'package:meteo_app/data/api/weather.api.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';
import 'package:meteo_app/presentation/widgets/languages/language.bloc.dart';
import 'package:meteo_app/presentation/widgets/languages/language.states.dart';

class LanguageListener extends StatelessWidget {
  final Widget child;
  final LanguageBloc _bloc = LanguageBloc();

  LanguageListener({required this.child});

  @override
  Widget build(BuildContext context) {
    serviceLocator<WeatherApi>().setLanguageCode(context.locale.languageCode);
    return BlocProvider<LanguageBloc>(
        create: (context) => _bloc,
        child: BlocListener<LanguageBloc, BlocState>(
            listener: (context, state) {
              if (state is LocaleChanged) {
                final newLocale = state.locale;
                context.setLocale(newLocale);
                serviceLocator<WeatherApi>().setLanguageCode(newLocale.languageCode);
              }
            },
            child: child));
  }
}
