import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo_app/presentation/common/bloc_state.dart';
import 'package:meteo_app/presentation/widgets/languages/language.states.dart';

class LanguageBloc extends Cubit<BlocState> {
  Locale? _locale;

  LanguageBloc() : super(Loading());

  void setLocale(Locale locale) {
    if (locale != _locale) {
      _locale = locale;
      emit(LocaleChanged(locale));
    }
  }
}
