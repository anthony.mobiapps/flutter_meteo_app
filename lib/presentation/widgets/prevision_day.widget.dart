import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:meteo_app/data/dtos/weather.dto.dart';
import "package:meteo_app/presentation/common/string.extension.dart";
import 'package:meteo_app/presentation/widgets/prevision_time.widget.dart';

class PrevisionDay extends StatelessWidget {
  final DateTime day;
  final List<Prevision> previsions;

  PrevisionDay({required this.day, required this.previsions});

  bool get _isToday => DateUtils.dateOnly(DateTime.now()).difference(day).inDays == 0;

  bool get _tomorrow => DateUtils.dateOnly(DateTime.now()).difference(day).inDays == -1;

  String dayLabel(Locale locale) => _isToday
      ? tr('weatherPage.today')
      : (_tomorrow ? tr('weatherPage.tomorrow') : DateFormat('EEEE', locale.languageCode).format(day));

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [Text(dayLabel(context.locale).capitalize()), _buildWeatherDay(context)],
    );
  }

  Widget _buildWeatherDay(BuildContext context) => SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: previsions
              .map((prevision) => SizedBox(
                    width: MediaQuery.of(context).size.width * 0.23,
                    child: PrevisionTime(prevision: prevision),
                  ))
              .toList(),
        ),
      );
}
