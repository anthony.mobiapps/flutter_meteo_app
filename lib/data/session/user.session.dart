import 'package:injectable/injectable.dart';
import 'package:meteo_app/domain/entities/user.entity.dart';

@singleton
class UserSession {
  User? _user;

  User get user => _user!;

  set user(User? value) {
    _user = value;
  }

  void clear() {
    this.user = null as User;
  }
}
