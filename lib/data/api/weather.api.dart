import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:meteo_app/config/parameters.dart';
import 'package:meteo_app/data/api/weather.client.dart';
import 'package:meteo_app/data/dtos/weather.dto.dart';
import 'package:meteo_app/domain/exceptions/api.exception.dart';
import 'package:retrofit/dio.dart';

@lazySingleton
class WeatherApi {
  Dio? _dio;
  String? _languageCode;
  WeatherClient? _client;

  void setLanguageCode(String languageCode) => _languageCode = languageCode;

  Future<WeatherResponse> getWeatherByCity(String cityName) async {
    _checkClient();
    return _checkResponse(_client!.getWeatherByCity(cityName));
  }

  Future<WeatherResponse> getWeatherByCoordinates(double latitude, double longitude) async {
    _checkClient();
    return _checkResponse(_client!.getWeatherByCoordinates(latitude, longitude));
  }

  Future<WeatherResponse> _checkResponse(Future<HttpResponse<WeatherResponse>> httpResponse) async {
    try {
      HttpResponse<WeatherResponse> response = await httpResponse;
      if (response.response.statusCode == 200) {
        return response.data;
      } else {
        throw ApiException.fromStatusCode(response.response.statusCode!);
      }
    } on DioError catch (error) {
      throw ApiException.fromStatusCode(error.response!.statusCode!);
    }
  }

  void _checkClient() {
    if (_client == null) {
      _dio = Dio();
      _dio!.options.queryParameters['appid'] = AppParameters.instance.apiId;
      _client = WeatherClient(_dio!);
    }

    _dio!.options.queryParameters['lang'] = _languageCode;
    _dio!.options.queryParameters['units'] = _languageCode == 'fr' ? 'metric' : '';
  }
}
