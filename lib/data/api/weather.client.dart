import 'package:dio/dio.dart';
import 'package:meteo_app/data/dtos/weather.dto.dart';
import 'package:retrofit/retrofit.dart';

part 'weather.client.g.dart';

@RestApi(baseUrl: 'http://api.openweathermap.org/data/2.5')
abstract class WeatherClient {

  factory WeatherClient(Dio dio, {String baseUrl}) = _WeatherClient;

  @GET('/forecast')
  Future<HttpResponse<WeatherResponse>>  getWeatherByCity(@Query('q') String cityName);

  @GET('/forecast')
  Future<HttpResponse<WeatherResponse>> getWeatherByCoordinates(@Query('lat') double latitude, @Query('lon') double longitude);
}
