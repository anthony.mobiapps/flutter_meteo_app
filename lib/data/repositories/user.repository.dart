import 'package:easy_localization/easy_localization.dart';
import 'package:injectable/injectable.dart';
import 'package:meteo_app/data/session/user.session.dart';
import 'package:meteo_app/domain/entities/user.entity.dart';
import 'package:meteo_app/domain/exceptions/repository.exception.dart';
import 'package:meteo_app/domain/repositories/iuser.repository.dart';

@Singleton(as: IUserRepository)
class UserRepository implements IUserRepository {
  UserSession session;

  UserRepository(this.session);

  @override
  Future<void> login(String email, String password) async {
    await Future.delayed(Duration(seconds: 2));

    /// Check email and password are ok!
    if (email != 'john.doe@mail.com' || password != 'mdp') {
      throw RepositoryException(tr('errors.badLogin'));
    }

    /// Store the user retrieved normally from the authentication system
    session.user =
        User(email: email, password: password, firstName: 'John', lastName: 'Doe', sessionToken: 'tokenGenerated');
  }

  @override
  Future<void> logout() => Future.delayed(Duration(milliseconds: 500), () => session.user = null);

  @override
  User getLoggedUser() => session.user;
}
