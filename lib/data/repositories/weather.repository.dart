import "package:collection/collection.dart";
import 'package:geolocator/geolocator.dart';
import 'package:injectable/injectable.dart';
import 'package:meteo_app/data/api/weather.api.dart';
import 'package:meteo_app/data/dtos/weather.dto.dart';
import 'package:meteo_app/domain/entities/weather.entity.dart';
import 'package:meteo_app/domain/repositories/iweather.repository.dart';
import 'package:meteo_app/presentation/common/utils.dart';

@Singleton(as: IWeatherRepository)
class WeatherRepository implements IWeatherRepository {
  WeatherApi _weatherApi;

  WeatherRepository(this._weatherApi);

  @override
  Future<WeatherModel?> getWeatherByCity(String cityName) async {
    final res = await _weatherApi.getWeatherByCity(cityName);
    return _extractData(res);
  }

  @override
  Future<WeatherModel?> getWeatherByPosition(Position position) async {
    final res = await _weatherApi.getWeatherByCoordinates(position.latitude, position.longitude);
    return _extractData(res);
  }

  WeatherModel _extractData(WeatherResponse res) {
    final previsions =
        groupBy<Prevision, DateTime>(res.previsions!, (prevision) => Utils.onlyDate(Utils.toDateTime(prevision.dt)));
    return WeatherModel(city: res.city!, previsions: previsions);
  }
}
