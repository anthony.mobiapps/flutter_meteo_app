import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:meteo_app/config/dependencies.dart';
import 'package:meteo_app/config/parameters.dart';
import 'package:meteo_app/root.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  configureDependencies();
  runZonedGuarded(
      () => runApp(EasyLocalization(
          supportedLocales: [Locale('en'), Locale('fr')],
          useOnlyLangCode: true,
          path: AppParameters.instance.i18nPath,
          fallbackLocale: Locale('en'),
          child: Root())), (Object error, StackTrace stackTrace) {
    print(error);
  });
}
