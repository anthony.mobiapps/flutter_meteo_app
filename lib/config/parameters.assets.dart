mixin AssetsParameters {
  final String pathImg = 'images/';

  final String apiIconsUrl = 'http://openweathermap.org/img/wn/';

  String get pathImgAssets => 'assets/' + pathImg;

  String get weatherIcon => pathImgAssets + 'weather-icon.png';

  String get weatherBackground => pathImgAssets + 'weather-background.jpeg';
}
