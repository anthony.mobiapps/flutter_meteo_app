// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../data/api/weather.api.dart' as _i3;
import '../data/repositories/user.repository.dart' as _i6;
import '../data/repositories/weather.repository.dart' as _i8;
import '../data/session/user.session.dart' as _i4;
import '../domain/repositories/iuser.repository.dart' as _i5;
import '../domain/repositories/iweather.repository.dart'
    as _i7; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.lazySingleton<_i3.WeatherApi>(() => _i3.WeatherApi());
  gh.singleton<_i4.UserSession>(_i4.UserSession());
  gh.singleton<_i5.IUserRepository>(_i6.UserRepository(get<_i4.UserSession>()));
  gh.singleton<_i7.IWeatherRepository>(
      _i8.WeatherRepository(get<_i3.WeatherApi>()));
  return get;
}
