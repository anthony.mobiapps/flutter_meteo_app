import 'package:meteo_app/config/parameters.assets.dart';

class AppParameters with AssetsParameters {
  static AppParameters? _instance;

  static AppParameters get instance {
    if (_instance == null) {
      _instance = AppParameters._();
    }
    return _instance!;
  }

  AppParameters._();

  String get i18nPath => 'assets/i18n';

  String get apiId => '252f3aa7652d48c55c7b3f2b0a8e1c26';
}
