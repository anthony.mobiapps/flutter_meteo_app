import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:meteo_app/config/dependencies.config.dart';

final serviceLocator = GetIt.instance;

@InjectableInit()
void configureDependencies() => $initGetIt(serviceLocator);