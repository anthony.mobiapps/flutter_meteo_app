# MeteoApp

Weather Application


## Login information
- email: john.doe@mail.com
- password: mdp

## Generate serialization part code with
`flutter pub run build_runner build --delete-conflicting-outputs`

## Features

<figure class="image">
  <img src="./doc_images/login.png" alt="Login" style="zoom:20%;" />
  <figcaption>Login screen</figcaption>
</figure>

<figure class="image">
  <img src="./doc_images/geolocation.png" alt="Geolocation Weather" style="zoom:20%;" />
  <figcaption>Geolocation weather screen</figcaption>
</figure>

<figure class="image">
  <img src="./doc_images/searchCity.png" alt="Search City Weather" style="zoom:20%;" />
  <figcaption>Search city weather screen</figcaption>
</figure>

<figure class="image">
  <img src="./doc_images/language.png" alt="Language Change" style="zoom:20%;" />
  <figcaption>Language Change screen</figcaption>
</figure>





